
package com.hohuy.mps.soap.subscribe;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the cm.cw package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _SubRequest_QNAME = new QName("http://ws.com/", "subRequest");
    private final static QName _SubRequestResponse_QNAME = new QName("http://ws.com/", "subRequestResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: cm.cw
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link SubRequest }
     * 
     */
    public SubRequest createSubRequest() {
        return new SubRequest();
    }

    /**
     * Create an instance of {@link SubRequestResponse }
     * 
     */
    public SubRequestResponse createSubRequestResponse() {
        return new SubRequestResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SubRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.com/", name = "subRequest")
    public JAXBElement<SubRequest> createSubRequest(SubRequest value) {
        return new JAXBElement<SubRequest>(_SubRequest_QNAME, SubRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SubRequestResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.com/", name = "subRequestResponse")
    public JAXBElement<SubRequestResponse> createSubRequestResponse(SubRequestResponse value) {
        return new JAXBElement<SubRequestResponse>(_SubRequestResponse_QNAME, SubRequestResponse.class, null, value);
    }

}
