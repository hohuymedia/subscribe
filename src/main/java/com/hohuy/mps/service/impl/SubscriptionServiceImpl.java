/**
 * 
 */
package com.hohuy.mps.service.impl;

import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.hohuy.mps.AppConfig;
import com.hohuy.mps.service.SubscriptionService;
import com.hohuy.mps.soap.subscribe.SubscribeService;
import com.hohuy.mps.soap.subscribe.SubRequest;
import com.hohuy.mps.soap.subscribe.Subscribe;
import com.hohuy.mps.util.Telco;
import com.hohuy.mps.util.ValidateUrl;

/**
 * @author giangdd
 *
 */
@Service("SendMonfeeServer")
public class SubscriptionServiceImpl implements SubscriptionService {
	private Logger logger = Logger.getLogger(SubscriptionServiceImpl.class);

	@Autowired
	private AppConfig cfg;

	@Autowired
	private Telco telco;
	
	@Value("${subscribe.threshold:100}")
	private int threshold;
	
	@PostConstruct
	private void load() {

	}

	@Override
	public void jobSendMonfee() {
		try {
			URL url = ValidateUrl.checkConnect(cfg.getUrl());
			if (url == null) {
				logger.fatal("Cannot connect to URL " + cfg.getUrl());
				return;
			}
			Random rand = new Random();
			SubscribeService subscribeClient = new SubscribeService(url);
			Subscribe request = subscribeClient.getSubscribePort();
			for (int i = 0; i < threshold; i++){
				String msisdn = telco.fullMsisdn(generatePhoneNumber());
				while (msisdn == null){
					msisdn = telco.fullMsisdn(generatePhoneNumber());
				}
				DateFormat fmt = new SimpleDateFormat("yyyyMMddkkmmss");
				String params = "" + rand.nextInt(1);
				String mode = rand.nextBoolean()?SubRequest.REAL:SubRequest.TEST;
				String result = request.subRequest (cfg.getUsername(), cfg.getPassword(), cfg.getServiceID(), msisdn, fmt.format(new Date()), params, mode, cfg.getAmount(), cfg.getCommand());
				logger.info("Got result from subscribeRequest: " + result);
			}
			logger.info("Successfully send " + threshold + " subscribe messages");
			
		} catch (Exception e) {
			logger.error("Exception:", e);
		}
	}
	
	private String generatePhoneNumber(){
		 int num1, num2, num3; //3 numbers in area code
	        int set2, set3; //sequence 2 and 3 of the phone number
	        
	        Random generator = new Random();
	        
	        //Area code number; Will not print 8 or 9
	        num1 = generator.nextInt(7) + 1; //add 1 so there is no 0 to begin  
	        num2 = generator.nextInt(8); //randomize to 8 becuase 0 counts as a number in the generator
	        num3 = generator.nextInt(8);
	        
	        // Sequence two of phone number
	        // the plus 100 is so there will always be a 3 digit number
	        // randomize to 643 because 0 starts the first placement so if i randomized up to 642 it would only go up yo 641 plus 100
	        // and i used 643 so when it adds 100 it will not succeed 742 
	        set2 = generator.nextInt(643) + 100;
	        
	        //Sequence 3 of numebr
	        // add 1000 so there will always be 4 numbers
	        //8999 so it wont succed 9999 when the 1000 is added
	        set3 = generator.nextInt(8999) + 1000;
	        return "" + num1 + num2 + num3 + set2 + set3;
	}
}
