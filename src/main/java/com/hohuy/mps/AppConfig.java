/**
 * 
 */
package com.hohuy.mps;

import java.io.Serializable;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * @author giangdd
 *
 */
@Component
@Scope("singleton")
public class AppConfig implements Serializable {
	private static final long serialVersionUID = -7605681936529899163L;
	private Logger logger = Logger.getLogger(AppConfig.class);
	private static AppConfig appConfig;

	/**
	 * Platform ShortCode
	 */
	@Value("${vtl.mps.shortcode}")
	private String smsShortcode;
	@Value("${vtl.mps.subscribe.url}")
	private String subscribeUrl;
	
	/**
	 * Platform Monfee Username & Password
	 */
	@Value("${vtl.mps.subscribe.username}")
	private String subscribeUsername;
	@Value("${vtl.mps.subscribe.password}")
	private String subscribePassword;
	
	@Value("${vtl.mps.subscribe.service}")
	private String serviceID;
	
	@Value("${vtl.mps.subscribe.command}")
	private String command;
	
	@Value("${vtl.mps.subscribe.amount:1000}")
	private String amount;

	private AppConfig() {
	}

	public static AppConfig getInstance() {
		return appConfig;
	}

	@PostConstruct
	private void load() {
		logger.info("========== SHORTCODE: " + this.smsShortcode + "==============");
		logger.info("========== SUBSCRIBE: " + this.subscribeUsername + "/" + this.subscribePassword + "==============");
	}


	public String getUrl() {
		return subscribeUrl;
	}

	public String getShortcode() {
		return smsShortcode;
	}

	public String getUsername() {
		return subscribeUsername;
	}

	public String getPassword() {
		return subscribePassword;
	}

	public String getSmsShortcode() {
		return smsShortcode;
	}

	public void setSmsShortcode(String smsShortcode) {
		this.smsShortcode = smsShortcode;
	}

	public String getMonfeeUrl() {
		return subscribeUrl;
	}

	public void setMonfeeUrl(String subscribeUrl) {
		this.subscribeUrl = subscribeUrl;
	}

	public String getMonfeeUsername() {
		return subscribeUsername;
	}

	public void setMonfeeUsername(String subscribeUsername) {
		this.subscribeUsername = subscribeUsername;
	}

	public String getMonfeePassword() {
		return subscribePassword;
	}

	public void setMonfeePassword(String subscribePassword) {
		this.subscribePassword = subscribePassword;
	}

	public String getServiceID() {
		return serviceID;
	}

	public void setServiceID(String serviceID) {
		this.serviceID = serviceID;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}
}
