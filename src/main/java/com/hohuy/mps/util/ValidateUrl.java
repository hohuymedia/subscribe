package com.hohuy.mps.util;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;

public class ValidateUrl {
	public static URL checkConnect(String address) {

		try {
			URL url = new URL(address);
			URLConnection conn = url.openConnection();
			conn.connect();

			return url;
		} catch (MalformedURLException e) {
			// the URL is not in a valid form
			return null;
		} catch (IOException e) {
			// the connection couldn't be established
			return null;
		}

	}

	public static URL getOneUrlActive(List<String> listAddress) {
		if(listAddress==null){
			return null;
		}
		for (String address : listAddress) {
			URL url = checkConnect(address);
			if (url != null) {
				return url;
			}
		}
		return null;
	}
}
