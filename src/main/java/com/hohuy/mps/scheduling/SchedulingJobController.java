/**
 * 
 */
package com.hohuy.mps.scheduling;

import java.io.Serializable;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.hohuy.mps.service.SubscriptionService;

/**
 * @author giangdd
 *
 */
@Component
public class SchedulingJobController implements Serializable {
	private static final long serialVersionUID = -7928486481330777195L;
	private Logger logger = Logger.getLogger(SchedulingJobController.class);
	private static Long threadIdSchedulingJob;

	private void setThreadIdJob() {
		synchronized (SchedulingJobController.class) {
			if (threadIdSchedulingJob == null) {
				threadIdSchedulingJob = Thread.currentThread().getId();
			}
		}

	}

	@Autowired
	private SubscriptionService monfeeClient;

	@Scheduled(fixedDelayString = "${monfee.delay:10000}")
	public void jobSendMonfee() {
		setThreadIdJob();
		if (threadIdSchedulingJob != null
				&& threadIdSchedulingJob == Thread.currentThread().getId()) {
			try {
				logger.info("Sending Monfee Request...");
				monfeeClient.jobSendMonfee();
			} catch (Exception e) {
				logger.error("Exception:", e);
			}
		}
	}
}
